/* view submodule for rawv
 * Copyright (C) 2010,2011,2012  Kirill Smelkov <kirr@navytux.spb.ru>
 * Copyright (C) 2011  Marine Bridge and Navigation Systems (http://mns.spb.ru/)
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include "rawv.h"

#include <stdio.h>
#include <SDL.h>
#include "sdlu.h"

using std::min;

namespace rawv {

/******** View ********/

View::View(int width, int height, bool upscale, const char *title)
{
    fprintf(stderr, "Viewing w: %i h: %i upscale: %i\n", width, height, upscale);

    if (SDL_InitSubSystem(SDL_INIT_VIDEO) < 0)
        die("SDL_INIT_VIDEO: %s", SDL_GetError());

    display = SDL_SetVideoMode(width, height, /*bpp=*/0, /*flags=*/0UL);
    if (!display)
        die("SDL_SetVideoMode: %s", SDL_GetError());

    if (title)
        SDL_WM_SetCaption(title, NULL);

    /*
     * FIXME pixfmt & w,h hardcoded, but better to adapt to incoming stream in
     * ->v_query_framebuf()
     */
    overlay = SDL_CreateYUVOverlay(width, height, SDL_YUY2_OVERLAY, display);
    if (!overlay)
        die("SDL_CreateYUVOverlay: %s", SDL_GetError());

    this->upscale = upscale;

}

View::~View()
{
    SDL_FreeYUVOverlay(overlay);
    SDL_QuitSubSystem(SDL_INIT_VIDEO);  // XXX needed at all?
}


/* non-forgiving helper for SDL_LockYUVOverlay();
 *
 * NOTE SDL_UnlockYUVOverlay returns void, so no need to check.
 */
static void SDLU_XLockYUVOverlay(SDL_Overlay *overlay)
{
    int err;

    err = SDL_LockYUVOverlay(overlay);
    if (err)
        die("view: SDL_LockYUVOverlay() failed");
}


bool View::v_query_framebuf(Frame *f)
{
    switch (f->pixfmt_4cc) {
        case MKTAG32('Y','U','Y','V'):
        case MKTAG32('Y','U','Y','2'):
            /* FIXME YUYV only because of hardcoded SDL_YUY2_OVERLAY */
            break;

        default:
            return false;
    }

    /* FIXME hardcoded, but can be relaxed */
    if ((f->width > overlay->w) || (f->height > overlay->h))
        return false;

    /* otherwise ok - we can provide framebuf for queried frame-metric */
    SDLU_XLockYUVOverlay(overlay);

    f->start = overlay->pixels[0];
    f->bytesperline = overlay->pitches[0];
    f->length = overlay->pitches[0] * overlay->h;

    return true;
}


/* upon receiving new frame display it */
void View::v_on_frame(const Frame *f)
{
#if 0
        double tstamp = buf->timestamp.tv_sec + 1E-6*buf->timestamp.tv_usec;

        fprintf(stderr, "buf[%i]  timestamp: %.7lf  sequence: %u\n", buf->index, tstamp, buf->sequence);
#endif

    SDL_Rect rsrc, rdst;
    int w, h;
    int i, j, istart;
    int bt;


    w = min(f->width,  overlay->w);
    h = min(f->height, overlay->h);

    /* no need to copy pixels if we provided this framebuf */
    if (f->start == overlay->pixels[0])
        goto display;


    bt = f->interlace_tb_swapped;
    if (bt)
        /* bt=+1    -> h=2k
         * bt=-1    -> h=2k-1
         */
        h = (h & ~1U) - int((1-bt)/2);

    /* for bt=-1, we have to start from 1, for bt=0,+1 - from 0'th line */
    istart = 0;
    if (bt==-1) {
        istart = 1;
        bt = -bt;   /* account for i 0->1 move */
    }


    SDLU_XLockYUVOverlay(overlay);

    switch (f->pixfmt_4cc) {
        case MKTAG32('Y','U','Y','V'):
        case MKTAG32('Y','U','Y','2'):

            if (
                overlay->pitches[0] == f->bytesperline &&
                overlay->w == f->width  && w == f->width &&
                overlay->h == f->height && h == f->height &&
                !bt
            )
            {
                /* special case for w=w, h=h, fmt=fmt, stride=stride, lines=01234... -- one large memcpy */
                memcpy(overlay->pixels[0], f->start, h*w * /*pixsize_yuy2*/ 2);
            }
            else {
                for (i=istart; i<h; ++i) {
                    memcpy(overlay->pixels[0] + (i+bt)*overlay->pitches[0],
                        f->start + i*f->bytesperline,
                        w * /*pixsize_yuy2*/ 2);

                    bt = -bt;
                }
            }

            break;

        case MKTAG32('U','Y','V','Y'):

            /* TODO optimize me through mmx */
            for (i=istart; i<h; ++i) {
                __u8 *dst = overlay->pixels[0] + (i+bt)*overlay->pitches[0];
                __u8 *src = f->start + i*f->bytesperline;

                for (j=0; j<w/2; j++) {
                    /* YUYV <- UYVY */
                    dst[0] = src[1];
                    dst[1] = src[0];
                    dst[2] = src[3];
                    dst[3] = src[2];

                    dst += 4;
                    src += 4;
                }

                bt = -bt;
            }

            break;

        case MKTAG32('Y','8','0','0'):
        case MKTAG32('Y','8',' ',' '):
        case MKTAG32('G','R','E','Y'):

            for (i=istart; i<h; ++i) {
                __u8 *dst = overlay->pixels[0] + (i+bt)*overlay->pitches[0];
                __u8 *src = f->start + i*f->bytesperline;

                for (j=0; j<w; ++j) {
                    dst[0] = src[0];
                    dst[1] = 0x80;  /* 0-level for Cb/Cr */

                    dst += 2;
                    src += 1;
                }

                bt = -bt;
            }

            break;

        default:
            die("view: W: unsupported pixfmt 0x%08x", f->pixfmt_4cc);
    }


display:
    SDL_UnlockYUVOverlay(overlay);

    /* blit overlay to its associated surface */
    rsrc.x = 0;
    rsrc.y = 0;
    rsrc.w = w;
    rsrc.h = h;

    rdst.x = 0;
    rdst.y = 0;

    if (!upscale) {
        rdst.w = w;
        rdst.h = h;
    }
    else {
        double ratio_w = (double)overlay->w / (w ? w : 1);
        double ratio_h = (double)overlay->h / (h ? h : 1);

        /* keep aspect ratio */
        double ratio = min(ratio_w, ratio_h);

        rdst.w = w * ratio;
        rdst.h = h * ratio;

        /* center picture */
        rdst.x = w * (ratio_w - ratio) / 2;
        rdst.y = h * (ratio_h - ratio) / 2;
    }

    SDLU_DisplayYUVOverlay(overlay, &rsrc, &rdst);
}


}   // rawv::
