/*  video capture for rawv
 *  Copyright (C) 2010  Kirill Smelkov <kirr@navytux.spb.ru>
 *  Copyright (C) 2011  Marine Bridge and Navigation Systems (http://mns.spb.ru/)
 *
 *  This library is free software: you can Use, Study, Modify and Redistribute
 *  it under the terms of the GNU Lesser General Public License version 2.1, or
 *  any later version. This library is distributed WITHOUT ANY WARRANTY. See
 *  COPYING.LIB file for full License terms.
 *
 *  V4L2 stuff, based on
 *  http://v4l2spec.bytesex.org/spec/capture-example.html
 */
#include "rawv.h"

#include <linux/videodev2.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>              /* low-level i/o */
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <byteswap.h>

#include <stdio.h>

#define CLEAR(x) memset (&(x), 0, sizeof (x))

namespace rawv {

/******** VideoCapture ********/

VideoCapture::VideoCapture(const char *dev_name, int width, int height, int interlace_tb_swapped, int queue_len)
{
    struct v4l2_capability cap;
    struct v4l2_format fmt;
    struct v4l2_requestbuffers req;
    unsigned i;
    char *p;

    vcapture_on = 0;

    fd = open (dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);
    if (-1 == fd)
        die ("Cannot open '%s': %d, %s", dev_name, errno, strerror (errno));

    if (-1 == ioctl (fd, VIDIOC_QUERYCAP, &cap))
        die_errno ("VIDIOC_QUERYCAP");

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
        die ("%s is no video capture device", dev_name);

    if (!(cap.capabilities & V4L2_CAP_STREAMING))
        die ("%s does not support streaming i/o", dev_name);


    /* Select video input, video standard and tune here. */
    CLEAR (fmt);

    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = width;
    fmt.fmt.pix.height      = height;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_NONE /*i.e. progressive*/;

    if (-1 == ioctl (fd, VIDIOC_S_FMT, &fmt)) {
            warn_errno ("VIDIOC_S_FMT(..., progressive) failed -- will try interlaced...");

            /* FIXME - better query dev capabilities in the first place */
            fmt.fmt.pix.field   = V4L2_FIELD_INTERLACED;
            if (-1 == ioctl (fd, VIDIOC_S_FMT, &fmt))
                die_errno ("VIDIOC_S_FMT(..., interlaced)");
    }



    /* Note VIDIOC_S_FMT may change width and height. */
    width  = fmt.fmt.pix.width;
    height = fmt.fmt.pix.height;

    p = (char *)&fmt.fmt.pix.pixelformat;
    fprintf(stderr, "Capturing  '%c%c%c%c'  h: %i  w: %i  stride: %i  bt: %i\n",
            p[0], p[1], p[2], p[3],
            fmt.fmt.pix.height, fmt.fmt.pix.width, fmt.fmt.pix.bytesperline,
            interlace_tb_swapped);


    /* setup mmap capture */
    CLEAR (req);

    req.count               = queue_len;
    req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory              = V4L2_MEMORY_MMAP;

    if (-1 == ioctl (fd, VIDIOC_REQBUFS, &req))
        die_errno("VIDIOC_REQBUFS");

    if (req.count < 2)
        die("Insufficient buffer memory on %s", dev_name);


    buffers.resize(req.count);

    for (i = 0; i < req.count; ++i) {
        struct v4l2_buffer buf;

        CLEAR (buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        if (-1 == ioctl (fd, VIDIOC_QUERYBUF, &buf))
            die_errno ("VIDIOC_QUERYBUF");

        buffers[i].width  = fmt.fmt.pix.width;
        buffers[i].height = fmt.fmt.pix.height;
        buffers[i].bytesperline = fmt.fmt.pix.bytesperline;

        /*
         * NOTE V4L2 constructs fourcc code for LE byteorder (see v4l2_fourcc
         * in linux/videodev2.h), so in order to transform pixfmt into
         * canonical order, we have to swap bytes on BE machines.
         */
        buffers[i].pixfmt_4cc = fmt.fmt.pix.pixelformat;
#if __BYTE_ORDER == __BIG_ENDIAN
        buffers[i].pixfmt_4cc = bswap_32(buffers[i].pixfmt_4cc);
#endif

        buffers[i].sequence = -1UL;

        buffers[i].interlace_tb_swapped = interlace_tb_swapped;

        buffers[i].length = buf.length;
        buffers[i].start  = (uint8_t *)
                mmap (NULL /* start anywhere */,
                      buf.length,
                      PROT_READ | PROT_WRITE /* required */,
                      MAP_SHARED /* recommended */,
                      fd, buf.m.offset);

        if (MAP_FAILED == buffers[i].start)
            die_errno ("mmap");
    }
}



VideoCapture::~VideoCapture()
{
    unsigned int i;

    /* munmap */
    for (i = 0; i < buffers.size(); ++i)
        if (-1 == munmap (buffers[i].start, buffers[i].length))
            die_errno ("munmap");


    if (-1 == close (fd))
        die_errno ("close");

    fd = -1;
}



void VideoCapture::v_start_capture()
{
    unsigned int i;
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (vcapture_on)
        return; /* already started */

    for (i = 0; i < buffers.size(); ++i) {
        struct v4l2_buffer buf;

        CLEAR (buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        if (-1 == ioctl (fd, VIDIOC_QBUF, &buf))
            die_errno ("VIDIOC_QBUF (v_start_capture)");
    }

    if (-1 == ioctl (fd, VIDIOC_STREAMON, &type))
        die_errno ("VIDIOC_STREAMON");

    vcapture_on = 1;
}


void VideoCapture::v_stop_capture()
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (!vcapture_on)
        return; /* already stopped */

    if (-1 == ioctl (fd, VIDIOC_STREAMOFF, &type))
        die_errno ("VIDIOC_STREAMOFF");

    vcapture_on = 0;
}



int VideoCapture::read_frame(void)
{
    struct v4l2_buffer buf;

    CLEAR (buf);

    buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == ioctl (fd, VIDIOC_DQBUF, &buf))
        switch (errno) {
            case EAGAIN:
                return 0;

            case EIO:
                /* Could ignore EIO, see spec. */

                /* fall through */

            default:
                die_errno ("VIDIOC_DQBUF");
        }

    assert (buf.index < buffers.size());

    buffers[buf.index].sequence = buf.sequence;

    /* notify subscribers */
    notify_v_subscribers(&buffers[buf.index]);

    if (-1 == ioctl (fd, VIDIOC_QBUF, &buf))
        die_errno ("VIDIOC_QBUF (read_frame)");

    return 1;
}

}   // rawv::
