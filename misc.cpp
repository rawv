/* misc utils for rawv
 * Copyright (C) 2010  Kirill Smelkov <kirr@navytux.spb.ru>
 * Copyright (C) 2011  Marine Bridge and Navigation Systems (http://mns.spb.ru/)
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include "rawv.h"

#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

namespace rawv {

RawvError::RawvError(const char *msg) : std::runtime_error(msg)
{}

RawvError::~RawvError() throw()
{}  /* emits vtable here */


void die(const char *fmt, ...)
{
    char msg[100];
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(msg, sizeof(msg), fmt, ap);
    throw RawvError(msg);
}

void die_errno(const char *s)
{
    die ("%s error %d, %s", s, errno, strerror (errno));
}

void warn(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
}

void warn_errno(const char *s)
{
    warn ("W: %s error %d, %s\n", s, errno, strerror (errno));
}


}   // rawv::
