/* rawv video source & sink interfaces, helpers
 * Copyright (C) 2012  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include "rawv.h"

namespace rawv {


IVideoSink::~IVideoSink()
{}

IVideoSource::~IVideoSource()
{}



VideoSource::VideoSource()
{
    vs_sinkbuf = NULL;
}

VideoSource::~VideoSource()
{}


void VideoSource::notify_v_subscribers(const Frame *f)
{
    unsigned i;

    for (i=0; i<v_subscribers.size(); ++i) {
        IVideoSink *vs = v_subscribers[i];

        /* sink which provided framebuffer should be notified last */
        if (vs == vs_sinkbuf)
            continue;

        vs->v_on_frame(f);
    }

    if (vs_sinkbuf) {
        vs_sinkbuf->v_on_frame(f);
        vs_sinkbuf = NULL;
    }
}


bool VideoSource::query_sink_framebuf(Frame *f)
{
    IVideoSink *vs;
    bool ok = false;
    unsigned i;

    if (vs_sinkbuf)
        die("VideoSource->query_sink_framebuf(): sinkbuf was already queried");


    for (i=0; i<v_subscribers.size(); ++i) {
        vs = v_subscribers[i];

        ok = vs->v_query_framebuf(f);
        if (ok) {
            vs_sinkbuf = vs;
            break;
        }
    }

    return ok;
}


void VideoSource::v_subscribe(IVideoSink *vs)
{
    v_subscribers.push_back(vs);
}


void VideoSource::v_unsubscribe(IVideoSink *vs)
{
    /* TODO handle unsubscribe of framebuf provider */
    if (vs == vs_sinkbuf)
        die("TODO VideoSource:  handle vs_sinkbuf unsubscription");

    for (vector<IVideoSink *>::iterator it=v_subscribers.begin(); it != v_subscribers.end();) {
        if (vs == *it)
            v_subscribers.erase(it);
        else
            ++it;
    }
}


}   // rawv::
