PREFIX	?= /usr/local

CC	:= gcc
CXX	:= g++
CFLAGS	:= -g -O2 -fPIC -Wall $(shell pkg-config --cflags sdl)
CXXFLAGS:= $(CFLAGS)
LDLIBS	:= $(shell pkg-config --libs sdl)

MAJOR	:= 0
MINOR	:= 0

all	: librawv.so rawv hrclock

librawv.so : ivideo.o view.o capture.o net.o misc.o sdlu.o
	$(LINK.cpp) -shared -Wl,-no-undefined -Wl,-soname=$@.$(MAJOR) $^ $(LDLIBS) -o $@.$(MAJOR).$(MINOR)
	/sbin/ldconfig -l $@.$(MAJOR).$(MINOR)
	ln -sf $@.$(MAJOR) $@

rawv	: rawv.o librawv.so
	$(LINK.cpp) $^ -Wl,-rpath=\$$ORIGIN -o $@

hrclock	: hrclock.o
	$(LINK.cpp) $^ $(LDLIBS) -lSDL_ttf -o $@


.PHONY	: clean
clean	:
	$(RM) *.o *.so *.so.* rawv hrclock

.PHONY	: install
install	: rawv.h librawv.so librawv.so.$(MAJOR) librawv.so.$(MAJOR).$(MINOR) rawv
	mkdir -p $(DESTDIR)$(PREFIX)/{include,lib,bin}/
	install -m 644 rawv.h $(DESTDIR)$(PREFIX)/include/
	cp -d librawv.so librawv.so.$(MAJOR) librawv.so.$(MAJOR).$(MINOR) $(DESTDIR)$(PREFIX)/lib/
	install rawv $(DESTDIR)$(PREFIX)/bin/
