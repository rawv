#ifndef _RAWV_H_
#define _RAWV_H_

/*
 *  RAWV - low-latency lossless video streaming over 1Gbps Ethernet
 *  Copyright (C) 2010  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 *  You may use and redistribute this work under the Creative Commons
 *  Attribution-Share Alike (CC-BY-SA) 3.0. You are free to Share (to copy,
 *  distribute, display, and perform the work) and to Remix (to make derivative
 *  works), under the following conditions:
 *
 *  1. Attribution. You must attribute the work in the manner specified by the
 *     author or licensor (but not in any way that suggests that they endorse you
 *     or your use of the work).
 *  2. Share Alike. If you alter, transform, or build upon this work, you may
 *     distribute the resulting work only under the same, similar or a compatible
 *     license.
 *
 *
 *  Abstract
 *  --------
 *
 *  A scheme for transmitting progressive PAL/NTSC video over 1Gbps Ethernet
 *  LAN is presented. Lossless coding, low end-to-end latencies (under 100ms in
 *  non-specialized test setup) and packet loss tolerance make this scheme
 *  applicable for specialized tasks in medical, industrial and military
 *  environments.  Development effort for both software and hardware
 *  implementations is shown to be low because of design simplicity.
 *
 *
 *  Coding
 *  ------
 *
 *  - each frame is transmitted in series of independent fragments
 *  - each fragment carries header and payload
 *  - header:
 *
 *    1. special 4-byte magic identifying this coding method,
 *    2. version of coding subvariant and fragment flags,
 *    3. fragment's frame sequence number,
 *    4. fragment sequence number and number of fragments in the whole frame,
 *    5. width, height and pixel format of its frame,
 *    6. fragment startline and number of lines in this fragment,
 *    7. padding;
 *
 *
 *                       < - - - - - - w i d t h - - - - - - >
 *                       +-----------------------------------+ ^
 *                       |                                   | |
 *                       |                                   | |
 *                       |                                   | |
 *                       |                                   |
 *     frag_startline -->|-----------------------------------| h
 *                       |  '                                | e
 *                       |  ' frag_nlines                    | i
 *                       |  '                                | g
 *                       |-----------------------------------| h
 *                       |                                   | t
 *                       |                                   |
 *                       |                                   | |
 *                       |                                   | |
 *                       |                                   | |
 *                       +-----------------------------------+ v
 *
 *
 *
 *           0                   1                   2                   3
 *           0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        0 |    0x52 ('R') |    0x41 ('A') |    0x57 ('W') |    0x56 ('V') |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       32 |    Version    |     Flags     |         Frame Number          |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       64 |       Fragment Number         |        Fragments Total        |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       96 |         Frame Width           |         Frame Height          |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      128 |                          Pixel Format                         |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      160 |     Fragment Startline        |        Fragment Lines         |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      192 |                            Reserved                           |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *      224 |                            Reserved                           |
 *          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *
 *    Version
 *       Version of coding used. This specification describes coding for
 *       version 0x01.
 *
 *    Flags
 *       This field is currently reserved to be 0x00.
 *
 *    Frame Number  (nframe)
 *       Sequence number of this fragment's frame.
 *
 *    Fragment Number  (nfragment)
 *       Sequence number of this fragment. Starts from 0 for each new frame.
 *
 *    Fragments Total  (fragments_total)
 *       Total number of fragments in current frame.
 *
 *    Frame Width  (width)
 *       Frame width in pixels.
 *
 *    Frame Height  (height)
 *       Frame height in pixels.
 *
 *    Pixel Format  (pixfmt)
 *       4 characters denoting payload pixel format according to
 *       http://fourcc.org/yuv.php; recommended are YUYV, YUY2, UYVY, GREY,
 *       Y800 and Y8.
 *
 *    Fragment Startline  (frag_startline)
 *       Vertical position of this fragment's first line relative to frame top.
 *
 *    Fragment Lines  (frag_nlines)
 *       Number of lines in this fragment.
 *
 *
 *    all integer fields are carried in network byte order, that is, most
 *    significant byte (octet) first.
 *
 *
 *  - payload:
 *
 *    1. right after header go raw pixel data in specified-in-header pixel format.
 *       There should be data for at least
 *
 *          frag_nlines * width                     (1)
 *
 *       pixels, which is
 *
 *          frag_nlines * width * pixsize(pixfmt)   (2)
 *
 *       bytes.
 *
 *    2. there could be more data than specified in (2). in such a case,
 *       trailer content is out of this specification scope.
 *
 *
 * TODO talk about packet reordering
 * TODO talk about packet loss
 * TODO talk about needed bandwidth for typical streams
 *       (PAL color, PAL mono, NTSC color, NTSC mono)
 *
 *
 * References:
 *
 * [1] RFC3550 -- RTP: A Transport Protocol for Real-Time Applications
 * [2] RFC3551 -- RTP Profile for Audio and Video Conferences with Minimal Control
 * [3] RFC4175 -- RTP Payload Format for Uncompressed Video
 * [4] IIDC 1394-based Digital Camera Specification Ver.1.32
 * [5] MPEG-TS (?)
 *
 */

/*
 *  RAWV library
 *  Copyright (C) 2010,2011,2012  Kirill Smelkov <kirr@navytux.spb.ru>
 *  Copyright (C) 2011  Marine Bridge and Navigation Systems (http://mns.spb.ru/)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <asm/types.h>
#include <sys/types.h>
#include <stdexcept>

#include <vector>
#include <arpa/inet.h>
#include <endian.h>

struct SDL_Surface;
struct SDL_Overlay;

namespace rawv {

using std::vector;


/* compose 4-characters tag to a 32-bit integer
 *
 * The mapping is so that host native in-memory representation of resulting
 * integer always repeats characters in originally specified order. e.g.
 *
 * MKTAG32('R','A','W','V') ->  0x56574152  (= "RAWV")  LE
 *                          ->  0x52415756  (= "RAWV")  BE
 *
 * that is integer is in native order, by character go in network byte-order
 * _always_.
 */
#define MKTAG32_LE(a,b,c,d) (((__u32)(a)<< 0)|((__u32)(b)<< 8)|((__u32)(c)<<16)|((__u32)(d)<<24))
#define MKTAG32_BE(a,b,c,d) (((__u32)(a)<<24)|((__u32)(b)<<16)|((__u32)(c)<< 8)|((__u32)(d)<< 0))

#if __BYTE_ORDER != __BIG_ENDIAN
# define MKTAG32    MKTAG32_LE
#else
# define MKTAG32    MKTAG32_BE
#endif


/*
 * RAWV packet header
 *
 * all fields go in network byteorder on the wire(*)
 */
#pragma pack(push, 1)
struct rawv_header {
    __u32   magic;  /* (*) fourcc -- _characters_ go in network byteorder */

    __u8    version;
    __u8    __reserved_for_flags;
    __u16   nframe;

    __u16   nfragment;
    __u16   fragments_total;

    __u16   width;
    __u16   height;

    __u32   pixfmt; /* (*) fourcc -- _characters_ go in network byteorder */

    __u16   frag_startline;
    __u16   frag_nlines;

    /* padding, so that the whole header is 32 bytes */
    __u32   __reserved[2];
};
#pragma pack(pop)

/*
 * One video frame
 */
struct Frame {
    __u8       *start;
    size_t      length; /* ~= height * bytesperline */

    int         width, height, bytesperline;
    __u32       pixfmt_4cc; /* characters go in network byteorder */
    __u32       sequence;
    // XXX timestamp ?

    /* flags */

    /* if we know top/bottom fields are in wrong order in this frame
     *
     * +1   -- swap fields
     * -1   -- swap and shift one filed XXX write more
     */
    signed int  interlace_tb_swapped    : 2;
};


/*
 * Interfaces for video sink and source
 */
struct IVideoSink {
    /* Source will query framebuffer from one of subscribed sinks, and if ok,
     * will put video frame directly into it. Otherwise source will use it's own
     * frame buffer.
     *
     * ->v_query_framebuf() should read
     *
     *      f->pixfmt_4cc,
     *      f->width,
     *      f->height
     *
     * to see whether it is possible to provide a buffer for requested
     * parameters, and if yes, have to set
     *
     *      f->start,
     *      f->length,
     *      f->bytesperline
     *
     * and return true.
     *
     * buffer should be valid until one next ->v_on_frame() call, after which
     * buffer ownership returns back to sink.
     * XXX interaction with v_unsubscribe()
     */
    virtual bool v_query_framebuf(Frame *f) = 0;

    /* Source will notify sink when new frame is available via this method */
    virtual void v_on_frame(const Frame *f) = 0;


    virtual ~IVideoSink();
};


struct IVideoSource {
    virtual void v_start_capture() = 0;
    virtual void v_stop_capture() = 0;

    virtual void v_subscribe(IVideoSink *) = 0;
    virtual void v_unsubscribe(IVideoSink *) = 0;

    virtual ~IVideoSource();
};


/* convenience class for IVideoSource implementers */
struct VideoSource : IVideoSource {
    void v_subscribe(IVideoSink *);
    void v_unsubscribe(IVideoSink *);


    vector<IVideoSink *> v_subscribers;
    IVideoSink *vs_sinkbuf; /* sink which provided framebuf */

    /* go through subscribers & notify them */
    void notify_v_subscribers(const Frame *f);

    /* query sinks for video framebuffer for f's frame-metric.
     *
     * see IVideoSink->v_query_framebuf() for details.
     */
    bool query_sink_framebuf(Frame *f);


    VideoSource();
    ~VideoSource();
};



/*
 * Displays video frames to YUY2 overlay
 */
struct View : IVideoSink
{
    View(int w, int h, bool upscale, const char *title);
    ~View();

    /* whether to upscale frame on display.
     *
     * NOTE: only upscale is supported, not downscale
     */
    bool upscale;

    /* IVideoSink */
    bool v_query_framebuf(Frame *f);
    void v_on_frame(const Frame *f);


    SDL_Surface *display;
    SDL_Overlay *overlay;
};


/*
 * Captures video frames from V4L2 source
 */
struct VideoCapture : VideoSource
{
    VideoCapture(const char *dev_name, int width, int height, int interlace_tb_swapped=0, int queue_len=4);
    ~VideoCapture();

    /* IVideoSource */
    void v_start_capture();
    void v_stop_capture();


    // XXX naming
    int  read_frame();



    int fd;

    vector<Frame> buffers;

    bool vcapture_on;

};


/*
 * Sends video frames to network
 */
struct NetTx : IVideoSink
{
    NetTx(const char *dest, int port, int mtu);
    ~NetTx();

    /* IVideoSink */
    bool v_query_framebuf(Frame *f);
    void v_on_frame(const Frame *f);

    int sk;
    struct sockaddr_in tx_addr;
    int mtu;
};


/*
 * Receives video frames from network
 */
struct NetRx : VideoSource
{
    NetRx(const char *listen_on, int port, int mtu);
    ~NetRx();

    /* IVideoSource */
    void v_start_capture();
    void v_stop_capture();

    int handle_recv();
    void __handle_recv(unsigned len);


    int sk;
    struct sockaddr_in rx_addr;
    int mtu;

    void flush_frame();

    vector<__u8> fragbuf;   // XXX align?
    vector<__u8> framebuf;  // XXX align?

    struct Frame f; /* current frame */

    /* current rx state */
    __u16 nframe, nfragment, fragments_total;
    __u16 frag_received;        /* # of fragments received in current frame */
    __u32 frag_dropped_total;   /* total # of dropped fragments */
};


/* XXX is this a good idea ? */
struct RawvError : std::runtime_error
{
    RawvError(const char *msg);
    ~RawvError() throw();
};

void die(const char *fmt, ...);
void die_errno(const char *msg);
void warn(const char *fmt, ...);
void warn_errno(const char *msg);

}   /* rawv:: */

#endif
