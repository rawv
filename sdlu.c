/* SDL utils
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 * Copyright (C) 2011  Marine Bridge and Navigation Systems (http://mns.spb.ru/)
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include "sdlu.h"

/* From SDL_yuvfuncs.h - last updated from SDL-1.2.14. KEEP IN SYNC.
 * TODO check SDL version at compile time.
 *
 * ---- 8< -----
 */
typedef struct SDL_VideoDevice SDL_VideoDevice;

#ifndef _THIS
#define _THIS	SDL_VideoDevice *_this
#endif
struct private_yuvhwfuncs {
    int (*Lock)(_THIS, SDL_Overlay *overlay);
    void (*Unlock)(_THIS, SDL_Overlay *overlay);
    int (*Display)(_THIS, SDL_Overlay *overlay, SDL_Rect *src, SDL_Rect *dst);
    void (*FreeHW)(_THIS, SDL_Overlay *overlay);
};

/* ---- 8< ----- */


/*
 * Determine SDL's current_video at runtime via temporarily faking overlay ->Lock hwfunc
 */
static int SDLU_YUVDetectCurrentVideo_ViaLock(SDL_VideoDevice *_this, SDL_Overlay *overlay)
{
    SDL_VideoDevice **pSDL_current_video = (SDL_VideoDevice **)overlay->hwdata;
    *pSDL_current_video = _this;    /* _this is set to current_video by SDL */
    return 0;   /* "locked" ok */
}

static SDL_VideoDevice *SDLU_YUVCurrentVideo(SDL_Overlay *overlay)
{
    SDL_VideoDevice *SDL_current_video;

    int  (*yuvlock)   (SDL_VideoDevice *, SDL_Overlay *);
    struct private_yuvhwdata *hwdata;

    yuvlock = overlay->hwfuncs->Lock;
    hwdata  = overlay->hwdata;

    overlay->hwdata         = (struct private_yuvhwdata *)&SDL_current_video;
    overlay->hwfuncs->Lock  = SDLU_YUVDetectCurrentVideo_ViaLock;

    SDL_current_video = NULL;
    SDL_LockYUVOverlay(overlay);

    if (!SDL_current_video) {
        fprintf(stderr, "Can't determine SDL_current_video\n");   /* should not happen */
        abort();
    }

    overlay->hwfuncs->Lock  = yuvlock;
    overlay->hwdata         = hwdata;

    return SDL_current_video;
}


int SDLU_DisplayYUVOverlay(SDL_Overlay *overlay, SDL_Rect *srcrect, SDL_Rect *dstrect)
{
    SDL_VideoDevice *video = SDLU_YUVCurrentVideo(overlay);

    /* Just do hwfuncs dispatch like in SDL_DisplayYUVOverlay, without clipping prologue */
    return overlay->hwfuncs->Display(video, overlay, srcrect, dstrect);
}
