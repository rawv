#ifndef _SDLU_H_
#define _SDLU_H_

/* SDL utils
 * Copyright (C) 2011  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include <SDL.h>

#ifdef __cplusplus
extern "C" {
#endif

/** like SDL_DisplayYUVOverlay, but supports both `srcrect` and `dstrect`
 *
 *  ... and does no rects clipping.
 *
 */
int SDLU_DisplayYUVOverlay(SDL_Overlay *overlay, SDL_Rect *srcrect, SDL_Rect *dstrect);




#ifdef __cplusplus
}
#endif


#endif
