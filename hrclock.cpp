/* high resolution clock display
 * Copyright (C) 2010  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 * This library is free software: you can Use, Study, Modify and Redistribute
 * it under the terms of the GNU Lesser General Public License version 2.1, or
 * any later version. This library is distributed WITHOUT ANY WARRANTY. See
 * COPYING.LIB file for full License terms.
 */

#include <SDL.h>
#include <SDL_ttf.h>

#include <unistd.h>

// XXX error handling

SDL_Surface *display;

TTF_Font    *font;
int          font_height;
SDL_Surface *glyphs[128];   /* first 20 will be NULL */

SDL_Color   fg;
SDL_Color   bg;


/*
 * Utils
 */
void die(const char *fmt, ...) __attribute__ ((noreturn));
void die(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    exit(1);
}

double microtime()
{
    return ((double)SDL_GetTicks()) / 1000;
}

void display_text(const char *s)
{
    SDL_Surface *g;
    SDL_Rect r;

    r.x = 0;
    r.y = 0;

    for (; *s!=0; ++s) {
        g = glyphs[(unsigned char)*s];
        r.w = g->w;
        r.h = g->h;
        r.y = font_height - g->h;
        SDL_BlitSurface(g, NULL/*whole glyph*/, display, &r);

        r.x += g->w ? g->w : 32;
    }

}

int main(int argc, char *argv[])
{
    int width = 300, height = 120;
    int ch;
    double now;

    const char *fontname = argv[1] ?
        argv[1]
        : "/usr/share/fonts/truetype/ttf-bitstream-vera/VeraMono.ttf";

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE);
    TTF_Init();

    font = TTF_OpenFont(fontname, 96);
    if (!font)
        die("TTF_OpenFont: %s\n", TTF_GetError());


    TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

    if (!TTF_FontFaceIsFixedWidth(font))
        fprintf(stderr, "W: non monospaced font...\n");

    font_height = TTF_FontHeight(font);


    display = SDL_SetVideoMode(width, height, /*bpp=*/0, /*flags=*/0UL);
    if (!display)
        die("SDL_SetVideoMode: %s\n", SDL_GetError());

    fg.r=0xff; fg.g=0xff; fg.b=0xff;
    bg.r=0x00; bg.g=0x00; bg.b=0x00;

    /* render characters */
    for (ch=20; ch<128; ++ch) {
        glyphs[ch] = TTF_RenderGlyph_Solid(font, ch, fg);
        if (!glyphs[ch])
            die("TTF_RenderGlyph_Solid\n");
    }



    while (1) {
        char tmp[80];

        SDL_FillRect(display, NULL, SDL_MapRGB(display->format, bg.r, bg.g, bg.b));

        now = microtime();

        //sprintf(tmp, "%.3lf %.3lf %.3lf", now, now, now);
        sprintf(tmp, "%.3lf", now);
        display_text(tmp);

        SDL_UpdateRect(display, 0,0,0,0);

        usleep(1000/*=1ms*/);
    }


    TTF_CloseFont(font);

    TTF_Quit();
    SDL_Quit();

    return 0;
}
