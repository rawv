/*
 *  RAWV demo
 *  Copyright (C) 2010  Kirill Smelkov <kirr@navytux.spb.ru>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  See COPYING.LIB file for full License terms.
 */

#include "rawv.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <getopt.h>             /* getopt_long() */

#include <errno.h>
#include <malloc.h>
#include <sys/time.h>

#include <string>


using namespace rawv;
using std::string;
using std::max;



/*
 * Globals
 */

View            *view  = NULL;
VideoCapture    *vcap  = NULL;
NetTx           *nettx = NULL;
NetRx           *netrx = NULL;



/****************************************/


void mainloop(void)
{
    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r, fdmax=-1;

        FD_ZERO (&fds);

        if (vcap) {
            FD_SET (vcap->fd, &fds);
            fdmax = max(fdmax, vcap->fd);
        }

        if (netrx) {
            FD_SET (netrx->sk, &fds);
            fdmax = max(fdmax, netrx->sk);
        }


        /* Timeout. */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        /* WARNING: select() with timeout under load is very expensive - for
         * almost every packet the timer will be set up which costs significant
         * overhead.
         *
         * Avoid that, but TODO later we'd better figure out how to get rid of
         * calling lots of select() at all.
         */
        (void)tv;
        r = select (fdmax + 1, &fds, NULL, NULL, /*&tv*/NULL);

        if (-1 == r) {
                if (EINTR == errno)
                        continue;

                die_errno ("select");
        }

        if (0 == r) {
            fprintf(stderr, "I: select timeout...\n");
            continue;
        }

        if (vcap && FD_ISSET(vcap->fd, &fds))
            if (vcap->read_frame ()) {
                /* frame processed */
                ; /* XXX what to do here? */
            }

        if (netrx && FD_ISSET(netrx->sk, &fds))
            netrx->handle_recv();

        /* EAGAIN - continue select loop. */
    }
}

void usage (FILE *fp, int argc, char *argv[])
{
    fprintf (fp,
"\
RAWV demo  (free software; comes with no warranty, but with best wishes)\n\
\n\
Usage: %s [options]\n\
\n\
Options:\n\
-d | --device name[,WxH][,btt?] Video device name [/dev/video]\n\
-v | --view[=WxH[,s]]           View video stream in SDL window\n\
-t | --nettx dest:port          Transmit video stream to dest\n\
-r | --netrx [group]:port       Receive video stream on port\n\
-h | --help                     Print this message\n\
\n\
Examples:\n\
$ rawv -d /dev/video -v                     # capture + view\n\
$ rawv -d /dev/video -t 127.0.0.255:port    # capture + send video over loopback\n\
$ rawv -r :port -v                          # receive video from network + view\n\
$ rawv -d /dev/video -v -t 127.0.0.255:port # capture + view + send video to network\n\
",
        argv[0]);
}

const char short_options [] = "d:t:r:v::h";

const struct option
long_options [] = {
        { "device",     required_argument,      NULL,           'd' },
        { "view",       optional_argument,      NULL,           'v' },
        { "nettx",      required_argument,      NULL,           't' },
        { "netrx",      required_argument,      NULL,           'r' },
        { "help",       no_argument,            NULL,           'h' },
        { 0, 0, 0, 0 }
};



int main2 (int argc, char *argv[])
{
        const char *dev_name = NULL;
        int video_width, video_height;
        int video_interlace_tb_swapped = 0;
        const char *nettx_dst = NULL, *netrx_group = NULL;
        int nettx_port = 0, netrx_port = 0;
        int view_width, view_height;
        bool view_upscale = 0;
        int do_view = 0;
        string title;

        int i;
        char *p, *q;


        /* default geometry */
        video_width  = view_width  = 640;
        video_height = view_height = 480;

        /* title */
        title = argv[0];
        for (i=1; i<argc; ++i) {
            title += " ";
            title += argv[i];
        }



        for (;;) {
                int index;
                int c;

                c = getopt_long (argc, argv,
                                 short_options, long_options,
                                 &index);

                if (-1 == c)
                        break;

                switch (c) {
                    case 0: /* getopt_long() flag */
                            break;

                    case 'd':
                            dev_name = optarg;
                            p = strchr(optarg, ',');
                            if (p) {
                                *p = 0;

                                p += 1;
                                q = strrchr(p, 'x');
                                if (!q)
                                    die("no 'x' in geometry spec (%s)", p);

                                *q = 0;

                                video_width  = atoi(p);
                                video_height = atoi(q+1);
                                if (!video_width || !video_height)
                                    die("can't parse video geometry (%s, %s)", p, q+1);

                                q = strchr(q+1, ',');
                                if (q && q[1]=='b' && q[2]=='t')
                                    video_interlace_tb_swapped = (q[3]=='t' ? -1 : +1);
                            }

                            break;

                    case 'v':
                            do_view = 1;

                            if (optarg) {
                                p = strrchr(optarg, 'x');
                                if (!p)
                                    die("no 'x' in geometry spec (%s)", optarg);

                                *p = 0;
                                view_width  = atoi(optarg);
                                view_height = atoi(p+1);
                                if (!view_width || !view_height)
                                    die("can't parse view geometry (%s, %s)", optarg, p+1);

                                p = strrchr(p+1, ',');
                                if (p && p[1]=='s')
                                    view_upscale = 1;
                            }

                            break;

                    case 't':
                            p = strrchr(optarg, ':');
                            if (!p)
                                die("no colon in tx spec (%s)", optarg);

                            *p = 0;

                            nettx_dst  = optarg;
                            nettx_port = atoi(p+1);
                            if (!nettx_port)
                                die("can't parse port in tx spec (%s)", p+1);

                            break;

                    case 'r':
                            p = strrchr(optarg, ':');
                            if (!p)
                                die("no colon in rx spec (%s)", optarg);

                            *p = 0;

                            if (strlen(optarg) != 0)
                                netrx_group = optarg;
                            netrx_port  = atoi(p+1);
                            if (!netrx_port)
                                die("invalid port '%s'", optarg);

                            break;

                    case 'h':
                            usage (stdout, argc, argv);
                            exit (0);


                    default:
                            usage (stderr, argc, argv);
                            exit (1);
                }
        }

        if (dev_name)
            vcap    = new VideoCapture(dev_name, video_width, video_height, video_interlace_tb_swapped);

        if (netrx_port)
            netrx   = new NetRx(netrx_group, netrx_port, /* FIXME mtu = */ 7200);

        if (do_view) {
            view    = new View(view_width, view_height, view_upscale, title.c_str());

            if (vcap)
                vcap  ->v_subscribe(view);
            if (netrx)
                netrx ->v_subscribe(view);
        }

        if (nettx_dst) {
            nettx   = new NetTx(nettx_dst, nettx_port, /* FIXME mtu = */ 7200);

            if (vcap)
                vcap  ->v_subscribe(nettx);
            if (netrx)
                netrx ->v_subscribe(nettx);
        }

        if (vcap)
            vcap->v_start_capture();
        // --//-- for netrx

        if (!vcap && !netrx && !view && !nettx)
            die("E: what should I do?");

        mainloop ();

        // --//-- for netrx
        if (vcap)
            vcap->v_stop_capture();


        if (netrx)
            delete netrx;
        if (nettx)
            delete nettx;
        if (view)
            delete view;
        if (vcap)
            delete vcap;

        return 0;
}


int main(int argc, char *argv[])
{
    try {
        return main2(argc, argv);
    }
    catch (const RawvError &e) {
        fprintf(stderr, "%s\n", e.what());
        return 1;
    }
}
